use std::convert::TryInto;

/// IDX file format supports the following data types. this enum is used wrap all possible data types and the vaules can be extracted using .try_into()
#[derive(Debug, Clone, PartialEq)]
pub enum RawData {
    UnsignedByte(u8),
    SignedByte(i8),
    Short(i16),
    Int(i32),
    Float(f32),
    Double(f64),
}

#[derive(Debug, Copy, Clone)]
pub enum Error {
    InvalidDataType,
}
impl TryInto<u8> for RawData {
    type Error = Error;
    fn try_into(self) -> Result<u8, Self::Error> {
        if let Self::UnsignedByte(x) = self {
            Ok(x)
        } else {
            Err(Error::InvalidDataType)
        }
    }
}
impl TryInto<i8> for RawData {
    type Error = Error;
    fn try_into(self) -> Result<i8, Self::Error> {
        if let Self::SignedByte(x) = self {
            Ok(x)
        } else {
            Err(Error::InvalidDataType)
        }
    }
}
impl TryInto<i16> for RawData {
    type Error = Error;
    fn try_into(self) -> Result<i16, Self::Error> {
        if let Self::Short(x) = self {
            Ok(x)
        } else {
            Err(Error::InvalidDataType)
        }
    }
}
impl TryInto<i32> for RawData {
    type Error = Error;
    fn try_into(self) -> Result<i32, Self::Error> {
        if let Self::Int(x) = self {
            Ok(x)
        } else {
            Err(Error::InvalidDataType)
        }
    }
}
impl TryInto<f32> for RawData {
    type Error = Error;
    fn try_into(self) -> Result<f32, Self::Error> {
        if let Self::Float(x) = self {
            Ok(x)
        } else {
            Err(Error::InvalidDataType)
        }
    }
}
impl TryInto<f64> for RawData {
    type Error = Error;
    fn try_into(self) -> Result<f64, Self::Error> {
        if let Self::Double(x) = self {
            Ok(x)
        } else {
            Err(Error::InvalidDataType)
        }
    }
}
